<?php
require_once("./cas.php");

$atr = CAS::authenticate();
if ($atr === false) {
    CAS::login();
}

?>

<p><a href="https://team.picasoft.net/fid-2021/channels/town-square">Cliquez ici</a> pour être redirigé vers le mattermost de la 3ème édition du FID</p>