# Site web low-tech du FID

https://assos.utc.fr/forumdelingenieriedurable

## Comment fonctionne le site ?

### L'architecture du site

Le site est composé d'une seule page html `index.html`. La navigation s'effectuent avec des ancrages (liens internes vers les différentes section du site).

Le style de la page est contenu dans le fichier `style.css`

Les fichiers `cas.php` et `protected_link.php` proviennent d'[ici](https://gitlab.utc.fr/chehabfl/CAS_UTC_PHP)
Ils permettent de mettre en place une authentification CAS pour accéder à un fichier (dans notre cas le fichier  `protected_link.php` qui protège le lien d'accès au Mattermost)

Les dossiers `img` et `files` contiennent respectivement les fichiers pdf et les images liées au site

### Mise en ligne

1. Pour modifier le site, il faut cloner le répertoire git hébergé sur `https://gitlab.utc.fr/burdyadr/site-web-fid`
2. Pour le mettre en ligne, on peut utiliser Filezilla.
	1. Connectez vous en VPN à l'utc (`vpnserv.utc.fr` avec openconnect par exemple sous linux)
	2. Transférez les fichiers sur le serveur (cf. [wiki des assos](https://assos.utc.fr/wiki/Acc%C3%A9der_%C3%A0_ses_donn%C3%A9es)). Les identifiants sont les même que ceux de la boîte mail


## Pourquoi un site web low-tech ?

### Qu'est-ce qu'un site web low-tech ?

Un site web low-tech, ça n'existe pas vraiment si on considère qu'Internet et l'informatique sont forcément high-tech. Néanmoins, il est possible de faire un site web qui soit moins complexe en termes de technologies (et souvent de fonctionnalités) afin de réduire son empreinte environnementale.

Ce site peut-être considéré comme low-tech car il utilise presque uniquement HTML et CSS, les technologies de base pour développer un site web et toutes les images sont compressées et leur nombre et leur taille ont été réduits au minimum pour réduire le poids de la page.

Quelques ressources supplémentaires :
- [conversion au low-tech](https://gauthierroussilhe.com/fr/posts/convert-low-tech)
- [mesurer l'impact d'un site](ecoindex.fr/)

### Astuces pour conserver un site web low-tech

- Garder une sobriété fonctionnelle ! Le plus important est de mettre de côté les animations et toutes les choses jolies, sympas, stylées mais futiles...
- Optimiser au maximum les images en privilégiant le svg quand c'est possible et en compressant les images (recommendation : https://tinyjpg.com/ et https://tinypng.com/)
- Eviter le javascript ! Il existe souvent des alternatives en CSS ou en HTML.

## Contacts

- Resp Info P21 : adrien.burdy@etu.utc.fr
- Resp Entreprise P21 : antoine.kryus@etu.utc.fr